package com.example.lynn.animate;

import android.content.Context;
import android.widget.Button;
import android.widget.RelativeLayout;

import static com.example.lynn.animate.MainActivity.*;

/**
 * Created by lynn on 6/16/2016.
 */
public class MyView extends RelativeLayout {

    public MyView(Context context) {
        super(context);

        setBackground(getResources().getDrawable(R.drawable.background));

        animate = new Button(context);

        animate.setText("Animate");

        animate.setOnClickListener(listener);

        button = new Button(context);

        button.setTranslationX(200);

        addView(animate);
        addView(button);
    }

}
